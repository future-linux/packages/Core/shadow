# Maintainer: Future Linux Team <future_linux@163.com>

pkgname=shadow
pkgver=4.11.1
pkgrel=2
pkgdesc="Password and account management tool suite with support for shadow files and PAM"
arch=('x86_64')
url='https://github.com/shadow-maint/shadow'
license=('BSD')
groups=('base')
depends=('glibc' 'libcap' 'linux-pam' 'cracklib')
makedepends=('acl' 'attr' 'bash' 'binutils' 'coreutils' 'diffutils' 'findutils' 'gawk'
   'gcc' 'gettext' 'grep' 'make' 'sed')
backup=(etc/default/useradd
    etc/pam.d/chage
    etc/pam.d/chfn
    etc/pam.d/chgpasswd
    etc/pam.d/chpasswd
    etc/pam.d/chsh
    etc/pam.d/groupadd
    etc/pam.d/groupdel
    etc/pam.d/groupmems
    etc/pam.d/groupmod
    etc/pam.d/login
    etc/pam.d/passwd
    etc/pam.d/su
    etc/pam.d/useradd
    etc/pam.d/userdel
    etc/pam.d/usermod
    etc/login.defs
    etc/login.defs.orig)
install=${pkgname}.install
source=(https://github.com/shadow-maint/shadow/releases/download/v${pkgver}/${pkgname}-${pkgver}.tar.xz
    chage
    chpasswd
    login
    passwd
    su
    useradd.defaults)
sha256sums=(41f093ce58b2ae5f389a1c5553e0c18bc73e6fe27f66273891991198a7707c95
    5619742a57a31ff2d44cca8551ee498b0555511e68e7d892ae2fee47f81428eb
    bce379fa76a44c30053a8fd3f536a717ae0cfabb18ed9a2d0af339f9b4fc6818
    45a3cb901e914865d157c1ec8be3522b58598d6469b9ddbe86682e1a73f2fbe1
    e2efb5767af8315d679faf8b1405d979e6807e7dd9daa76ce6be804820de9772
    cf76e2ebeef461a0de36905afb5f48aeaaaff2770eb0ee7a87fe758d6b7611e7
    069b6d66b638cb70e28c5594193b86bcdaed9ee1a987bc4e2ec82f0e6e01d3b1)

prepare() {
    cd ${pkgname}-${pkgver}

    sed -i 's/groups$(EXEEXT) //' src/Makefile.in
    find man -name Makefile.in -exec sed -i 's/groups\.1 / /'   {} \;
    find man -name Makefile.in -exec sed -i 's/getspnam\.3 / /' {} \;
    find man -name Makefile.in -exec sed -i 's/passwd\.5 / /'   {} \;

    sed -e 's:#ENCRYPT_METHOD DES:ENCRYPT_METHOD SHA512:' \
        -e 's:/var/spool/mail:/var/mail:'                 \
        -e '/PATH=/{s@/sbin:@@;s@/bin:@@}'                \
        -i etc/login.defs

    sed -i 's@DICTPATH.*@DICTPATH\t/lib64/cracklib/pw_dict@' etc/login.defs
}

build() {
    cd ${pkgname}-${pkgver}

    ${configure}            \
        --sysconfdir=/etc   \
        --bindir=/usr/bin   \
        --sbindir=/usr/sbin \
        --disable-static    \
        --with-libcrack     \
        --with-libpam       \
        --with-group-name-max-length=32

    make
}

package() {
    cd ${pkgname}-${pkgver}

    make DESTDIR=${pkgdir} install
    make -C man DESTDIR=${pkgdir} install-man

    install -vDm644 ${srcdir}/useradd.defaults ${pkgdir}/etc/default/useradd

    install -vm644 ${pkgdir}/etc/login.defs ${pkgdir}/etc/login.defs.orig
    for FUNCTION in FAIL_DELAY               \
                    FAILLOG_ENAB             \
                    LASTLOG_ENAB             \
                    MAIL_CHECK_ENAB          \
                    OBSCURE_CHECKS_ENAB      \
                    PORTTIME_CHECKS_ENAB     \
                    QUOTAS_ENAB              \
                    CONSOLE MOTD_FILE        \
                    FTMP_FILE NOLOGINS_FILE  \
                    ENV_HZ PASS_MIN_LEN      \
                    SU_WHEEL_ONLY            \
                    CRACKLIB_DICTPATH        \
                    PASS_CHANGE_TRIES        \
                    PASS_ALWAYS_WARN         \
                    CHFN_AUTH ENCRYPT_METHOD \
                    ENVIRON_FILE
    do
        sed -i "s/^${FUNCTION}/# &/" ${pkgdir}/etc/login.defs
    done

    for f in chage chpasswd login passwd su
    do
        install -vDm644 ${srcdir}/${f} ${pkgdir}/etc/pam.d/${f}
    done

    for PROGRAM in chfn chgpasswd chsh groupadd groupdel \
                    groupmems groupmod useradd userdel usermod
    do
        install -v -m644 ${pkgdir}/etc/pam.d/chage ${pkgdir}/etc/pam.d/${PROGRAM}
        sed -i "s/chage/${PROGRAM}/" ${pkgdir}/etc/pam.d/${PROGRAM}
    done
}
